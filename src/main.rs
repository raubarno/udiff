extern crate clap;

use clap::Parser;
use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::path::PathBuf;

#[derive(Parser)]
struct Args {
    path: PathBuf,
}

fn reader_to_iter<T>(re: BufReader<T>) -> impl Iterator<Item = (String, MapValue)>
where
    T: std::io::Read,
{
    re.lines()
        .flatten()
        .filter_map(|x: String| {
            let y = x.split_once(' ');
            y.and_then(|z| {
                if z.0.is_empty() {
                    None
                } else {
                    Some((String::from(z.0), String::from(z.1)))
                }
            })
        })
        .enumerate()
        .map(|(lnum, (lhs, rhs))| (lhs, MapValue { rhs, lnum }))
}

#[derive(Clone, Copy, Debug)]
enum DiffType {
    None,
    Add,
    Delete,
    Modify,
}

#[derive(Clone, Debug)]
struct ResultValue<'a> {
    head: &'a String,
    body: &'a String,
    diff: DiffType,
}

#[derive(Clone, Debug)]
struct MapValue {
    rhs: String,
    lnum: usize,
}

fn main() {
    let args = Args::parse();
    let file = File::open(&args.path).expect("Cannot open file");
    let lh_reader = BufReader::new(file);
    let lh_vec = reader_to_iter(lh_reader).collect::<Vec<_>>();
    let lh = lh_vec.clone().into_iter().collect::<HashMap<_, _>>();
    if lh.len() < lh_vec.len() {
        eprintln!("Error: the source file contains duplicate lines");
        std::process::exit(1);
    }
    let rh_reader = BufReader::new(std::io::stdin());
    let rh = reader_to_iter(rh_reader).collect::<Vec<_>>();
    let mut narr: Vec<Option<ResultValue>> = vec![None; lh.len()];
    for i in rh.iter() {
        if let Some(lh_entry) = lh.get(&i.0) {
            narr[lh_entry.lnum] = Some(ResultValue {
                head: &i.0,
                body: &i.1.rhs,
                diff: if i.1.rhs == lh_entry.rhs {
                    DiffType::None
                } else {
                    DiffType::Modify
                },
            })
        } else {
            narr.push(Some(ResultValue {
                head: &i.0,
                body: &i.1.rhs,
                diff: DiffType::Add,
            }))
        }
    }
    let diffs = narr
        .iter()
        .enumerate()
        .map(|(i, x): (usize, &Option<ResultValue>)| {
            if let Some(r) = x {
                (r.head, r.body, r.diff)
            } else {
                (&lh_vec[i].0, &lh_vec[i].1.rhs, DiffType::Delete)
            }
        }); //.collect::<Vec<_>>();
    let purediffs = diffs /*.iter()*/
        .filter(|x| !matches!(x.2, DiffType::None));
    for i in purediffs {
        println!(
            " {} {} {}",
            match i.2 {
                DiffType::Add => '+',
                DiffType::Delete => '-',
                DiffType::Modify => '~',
                _ => unreachable!(),
            },
            i.0,
            i.1
        );
    }
}
