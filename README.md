# udiff

Shell utility like GNU diff designed for an unordered list of key-value pairs; useful replacement for `sort | diff`.